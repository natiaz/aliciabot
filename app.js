var fs      = require('fs');
var Twit    = require('twit');
var options = require("./options.js");
var Twitter = new Twit(options.twitter);
var botname = options.user;


var stream = Twitter.stream('user');

stream.on('tweet', tweetEvent);

function tweetEvent(tweet) {
  var reply_to  = tweet.in_reply_to_screen_name;
  var name      = tweet.user.screen_name;
  var mention   = tweet.id_str;
  var txt       = tweet.text;



  if (reply_to === botname && name != botname) {       
    response(name);            
  }
  else
  {
    if(txt.indexOf(botname) > -1 && name != botname)
    {
      response(name); 
    } 
  } 
}

function response(name) {
  Twitter.get('statuses/user_timeline', { screen_name: name,count: 50 }, getSentiment); 
}

function getSentiment(err,data) {  
  if(!err)
  {  
    var text    = '';
    var name    = '';
    var mention = '';

    for (var i = 0; i < data.length ; i++) {    
      text += ' '+data[i].text;
      if(name == '')
      {
        name = data[i].user.screen_name;
      }
      if(data[i].in_reply_to_screen_name === botname)
      {
        mention = data[i].id_str;
      }
    }

    var reply_data = {
      senti:    mood(text),
      reply_to: name,
      mention:  mention
    };   

    postImage(reply_data);
  }
}

function mood(text) {
  var afinn = require('./moodwords.js');
  var tokens      = text.replace(/[^a-zñÑáéíóúÁÉÍÓÚA-Z- ]+/g, '').replace('/ {2,}/',' ').toLowerCase().split(' '),
      score       = 0,
      comparative = 0,
      words       = [],
      positive    = [],
      negative    = []; 

  var len = tokens.length;
  while (len--) { 
    var obj = tokens[len];
    var item = parseInt(afinn[obj]);
    if (!afinn.hasOwnProperty(obj)) continue;

    words.push(obj);
    if (item > 0) positive.push(obj);
    if (item < 0) negative.push(obj);

    score += item;
  }

  comparative = Math.round((100*positive.length) / words.length);
  var image   = setImage(comparative);
  var result = {
    score:    score,
    image:    image.imagen,
    status:   image.texto,
    altxt:    image.alt,    
    words:    words,
    positive: positive,
    negative: negative
  };

  return result;
}

function setImage(positive) {

  if(positive < 50)
  {
    var redondear = 100 - positive;
    var imagen    = './images/malo';
    var align     = 'malo';
    var diff 			= 'negativos que positivos';
  }
  else
  {
    var redondear = positive;
    var imagen    = './images/bueno';
    var align     = 'bueno';
    var diff 			= 'positivos que negativos';
  }

  var resto = redondear % 5;
  imagen   += (redondear - resto)+'.jpg';
  var alt 	= 'Eres un '+(redondear - resto)+align;

  if(typeof imagen === 'undefined' || imagen === null || imagen == './images/malo50.jpg')
  {
    imagen = './images/bueno50.jpg';
  }
    
  var msg = Math.round(Math.random() * (3 - 1) + 1);
  
  if(msg == 1)
 	{
		var texto = ' El espejo mágico te ha analizado y estás en el bando de los '+align+'s';
 	}
  if(msg == 2)
  {
  	var texto = ' Hemos analizado tu perfil y has escrito más tweets '+diff+'. Tu equipo está claro';
  }
  if(msg == 3)
  {
  	var texto = ' Este es tu resultado. Consulta a @_espejito_ siempre que desees saber la maldad de tu perfil.'
  }
  
  if(imagen == './images/bueno50.jpg')
  {
  	var texto = ' Las fuerzas están equilibradas en ti. La decisión de ser bueno o malo es tuya.'
  }
  
  var resultado = {imagen: imagen, texto: texto, alt: alt};

  return resultado;
}

function postImage(datos) {  
  var b64content = fs.readFileSync(datos.senti.image, { encoding: 'base64' });
  
  Twitter.post('media/upload', { media_data: b64content }, function (err, data, response) {
    var mediaIdStr  = data.media_id_string;
    var altText     = datos.senti.altxt;
    var meta_params = { media_id: mediaIdStr };
    var timest      = Math.floor(Date.now() / 1000);
    var reply       = '@'+datos.reply_to+datos.senti.status;
   
    Twitter.post('media/metadata/create', meta_params, function (err, data, response) {
      if (!err) {        
        var params = { 
          in_reply_to_status_id: datos.mention,
          status: reply, 
          media_ids: [mediaIdStr] 
        };
        Twitter.post('statuses/update', params, function (err, data, response) {
          
        })
      }
    });
  });
}
